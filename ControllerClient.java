package Controllers;

import DAO.*;
import Model.Client;

import PresentationLayer.ClientView;
import org.omg.PortableInterceptor.ACTIVE;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class ControllerClient {

    private ClientView clientView;
    private ClientDAO clientDAO;

    public ControllerClient(ClientView clientView,ClientDAO clientDAO){
        this.clientView = clientView;
        this.clientDAO = clientDAO;
        clientView.refresh(clientDAO.findAll());
        clientView.viewAllClientListener_m(new viewAllClientListener()); //View all clients
        clientView.addClientListener_m( new addClientListener());        //add client
        clientView.deleteClientListener_m( new deleteClientListener());     // delete client
        clientView.editClientListener_m(new editClientListener());          // edit client

    }

    private class viewAllClientListener implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            clientView.refresh(clientDAO.findAll());
        }
    }
    private class addClientListener implements ActionListener{

        public void actionPerformed(ActionEvent e) {

            clientDAO.insertClient(clientView.getFirstName(),clientView.getLastName());
            clientView.refresh(clientDAO.findAll());

        }
    }
    private class deleteClientListener implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            clientDAO.deleteById(clientView.getIdTF());
            clientView.refresh(clientDAO.findAll());
        }
    }
    private class editClientListener implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            clientDAO.updateClient(clientView.getIdTF(),clientView.getFirstName(),clientView.getLastName());
            clientView.refresh(clientDAO.findAll());
        }
    }


    public static void main(String[] args) {
        ControllerClient c =new ControllerClient(new ClientView(),new ClientDAO());
    }


}
