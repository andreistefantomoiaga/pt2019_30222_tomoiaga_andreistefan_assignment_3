package Model;

public class Product {

    private int id;
    private String productName;
    private int quantity;

    public int getId() {
        return id;
    }
    public String getProductName() {
        return productName;
    }
    public int getQuantity() {
        return quantity;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
