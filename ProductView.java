package PresentationLayer;

import Model.Client;
import Model.Product;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ProductView extends JFrame {

    JPanel pane=new JPanel(new BorderLayout(5,5));;
    JPanel top = new JPanel();
    JPanel center = new JPanel();
    JPanel bottom = new JPanel();

    JButton addProduct = new JButton("Add Product");
    JButton editProduct = new JButton("Edit Product");
    JButton deleteProduct = new JButton("Delete Product");
    JButton viewAllProducts = new JButton("View All Products");
    JLabel idSelectionLabel = new JLabel("ID: ");
    JTextField idTF = new JTextField("X");
    JLabel productName = new JLabel("Product Name: ");
    JTextField productNameTF = new JTextField("insert productName");
    JLabel quantityLabel = new JLabel("Quantity: ");
    JTextField quantityTF = new JTextField("insert quantity");

    JTable productTable=null;
    JScrollPane tablePane ;

    public  ProductView(){

        others();
        init();
        this.setSize(500,600);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(560,dim.height/2 - this.getSize().height/2);
        this.setVisible(true);
    }

    private void others(){

        center.setBorder(BorderFactory.createLineBorder(Color.BLACK));
    }

    private void init(){

        pane.add(top,BorderLayout.PAGE_START);
        pane.add(center,BorderLayout.CENTER);
        pane.add(bottom,BorderLayout.PAGE_END);
        top.add(addProduct);
        top.add(editProduct);
        top.add(deleteProduct);
        top.add(viewAllProducts);
        bottom.add(idSelectionLabel);
        bottom.add(idTF);
        bottom.add(productName);
        bottom.add(productNameTF);
        bottom.add(quantityLabel);
        bottom.add(quantityTF);
        this.add(pane);
    }


    public void refresh(List<Product> all){

        System.out.println("Clicked Product");
        pane.removeAll();
        center.removeAll();

        productTable  = createTable(all);
        tablePane = new JScrollPane(productTable);
        center.add(tablePane);
        init();

        pane.revalidate();
        pane.repaint();

    }

    public JTable createTable(List<Product> objects){

        List<String> columnNames = new ArrayList<String>();
        List<String[]> data = new ArrayList<String[]>();

        for (Field field: Product.class.getDeclaredFields()) {
            columnNames.add(field.getName());
            System.out.println(field.getName());
        }

        if(!objects.isEmpty()) {
            for (Product prod : objects) {
                data.add(new String[]{String.valueOf(prod.getId()), prod.getProductName(), String.valueOf(prod.getQuantity())});
            }
        }
        System.out.println();
        TableModel tableModel= new DefaultTableModel(data.toArray(new Object[][]{}),columnNames.toArray());
        JTable retunedJtable =new JTable(tableModel);

        return retunedJtable;
    }

    public void addProduct(ActionListener e){
        addProduct.addActionListener(e);
    }
    public void editProduct(ActionListener e){
        editProduct.addActionListener(e);
    }
    public void deleteProduct(ActionListener e){
        deleteProduct.addActionListener(e);
    }
    public void viewProducts(ActionListener e){
        viewAllProducts.addActionListener(e);
    }


    public static void main(String[] args) {

        ProductView c = new ProductView();
    }

    public String getProductNameFromTF(){
        return productNameTF.getText();
    }

    public int getQuantity(){
        return Integer.parseInt(quantityTF.getText());
    }

    public int getId(){
        return Integer.parseInt(idTF.getText());
    }
}
