import BusinessLogic.BusinessLogic;
import Controllers.ControllerClient;
import Controllers.ControllerOrder;
import Controllers.ControllerProduct;
import DAO.ClientDAO;
import DAO.OrderDAO;
import DAO.OrderDetailDAO;
import DAO.ProductDAO;
import PresentationLayer.ClientView;
import PresentationLayer.OrderView;
import PresentationLayer.ProductView;

public class AppLauncher {

    public AppLauncher(){

    }

    public static void main(String[] args) {

        ControllerClient c =new ControllerClient(new ClientView(),new ClientDAO());
        ControllerProduct ctrl = new ControllerProduct(new ProductView(),new ProductDAO());
        ControllerOrder co = new ControllerOrder(new OrderView(),new ClientDAO(),new ProductDAO(),new OrderDAO(),new OrderDetailDAO(),new BusinessLogic(),ctrl.getProductView());

    }


}
