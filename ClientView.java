package PresentationLayer;

import Model.Client;
import sun.management.snmp.jvminstr.JvmThreadInstanceEntryImpl;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ClientView extends JFrame {

    JPanel pane  = new JPanel(new BorderLayout(5,5));
    JPanel top = new JPanel();
    JPanel center = new JPanel();
    JPanel bottom = new JPanel();
    JButton addClient = new JButton("Add Client");
    JButton editClient = new JButton("Edit Client");
    JButton deleteClient = new JButton("Delete Client");
    JButton viewAllClients = new JButton("View Clients");
    JLabel idSelectionLabel = new JLabel("ID: ");
    JTextField idTF = new JTextField("X");
    JLabel firstName = new JLabel("firstName: ");
    JTextField firstNameTF = new JTextField("insert firstName");
    JLabel lastName = new JLabel("lastName: ");
    JTextField lastNameTF = new JTextField("insert lastName");


    JTable clientTable;
    JScrollPane tablePane;

    public ClientView(){

        others();
        init();
        this.setSize(500,600);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(50,dim.height/2 - this.getSize().height/2);
        this.setVisible(true);
    }

    private void init(){

       pane.add(top,BorderLayout.PAGE_START);
       pane.add(center,BorderLayout.CENTER);
       pane.add(bottom,BorderLayout.PAGE_END);
       top.add(addClient);
       top.add(editClient);
       top.add(deleteClient);
       top.add(viewAllClients);
       bottom.add(idSelectionLabel);
       bottom.add(idTF);
       bottom.add(firstName);
       bottom.add(firstNameTF);
       bottom.add(lastName);
       bottom.add(lastNameTF);
       this.add(pane);

    }

    private void others(){

        center.setBorder(BorderFactory.createLineBorder(Color.BLACK));
    }


    public void refresh(List<Client> all){

        System.out.println("Clicked");
        pane.removeAll();
        center.removeAll();


        clientTable  = createTable(all);
        tablePane = new JScrollPane(clientTable);
        center.add(tablePane);
        init();

        pane.revalidate();
        pane.repaint();

    }

    public JTable createTable(List<Client> objects){

       List<String> columnNames = new ArrayList<String>();
       List<String[]> data = new ArrayList<String[]>();

        for (Field field: Client.class.getDeclaredFields()) {
            columnNames.add(field.getName());
            System.out.println(field.getName());
        }

        if(!objects.isEmpty()) {
            for (Client client : objects) {
                data.add(new String[]{String.valueOf(client.getId()), client.getFirstName(), client.getLastName()});
            }
        }

        TableModel tableModel= new DefaultTableModel(data.toArray(new Object[][]{}),columnNames.toArray());
        JTable retunedJtable =new JTable(tableModel);

        return retunedJtable;
    }

    public void addClientListener_m(ActionListener e){
        addClient.addActionListener(e);
    }
    public void editClientListener_m(ActionListener e){
        editClient.addActionListener(e);
    }
    public void deleteClientListener_m(ActionListener e){
        deleteClient.addActionListener(e);
    }
    public void viewAllClientListener_m(ActionListener e){
        viewAllClients.addActionListener(e);
    }

//    public void clickListener_m(MouseListener e){
//        clientTable.addMouseListener();
//    }

    public int getIdTF(){
        return Integer.valueOf(idTF.getText());
    }
    public String getFirstName(){
        return firstNameTF.getText();
    }
    public String getLastName(){
        return lastNameTF.getText();
    }

    public void setTextFieldsFromModelTable(){
        //TODO: when clicked autofill textfields
        idTF.setText("text");
        idTF.setText("fname");
        idTF.setText("lname");
    }

    public static void main(String[] args) {

        ClientView c = new ClientView();
    }
}
