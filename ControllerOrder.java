package Controllers;

import BusinessLogic.BusinessLogic;
import DAO.ClientDAO;
import DAO.OrderDAO;
import DAO.OrderDetailDAO;
import DAO.ProductDAO;
import Model.Order;
import Model.OrderDetail;
import PresentationLayer.OrderView;
import PresentationLayer.ProductView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

// TODO: bussiness logic;


public class ControllerOrder {

    private OrderView orderView;
    private ClientDAO clientDAO;
    private ProductDAO productDAO;
    private OrderDAO orderDAO;
    private OrderDetailDAO orderDetailDAO;
    private BusinessLogic businessLogic;
    private ProductView productView;
    List<OrderDetail> nullList = new ArrayList<OrderDetail>();

    public ControllerOrder(OrderView orderView, ClientDAO clientDAO, ProductDAO productDAO, OrderDAO orderDAO, OrderDetailDAO orderDetailDAO, BusinessLogic businessLogic,ProductView productView){

        this.orderView = orderView;
        this.clientDAO = clientDAO;
        this.productDAO = productDAO;
        this.orderDAO = orderDAO;
        this.orderDetailDAO = orderDetailDAO;
        this.businessLogic = businessLogic;
        this.productView = productView;
        orderView.refresh(orderDetailDAO.findAll());


        //listeners :
        orderView.orderEvent(new OrderListener());
        orderView.showEvent(new ShowListener());
        orderView.createBillEvent(new createBillListener());



    }

    private class OrderListener implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            System.out.println("Pressed Order");

            int understockFlag = businessLogic.underStock(orderView.quantityText(),orderView.getIdProductText());

            if( understockFlag == 1){
                System.out.println("flag is 1");
                orderDetailDAO.inserOrderDetail(orderView.getIdProductText(),orderView.getidOrderText(),orderView.quantityText(),orderView.getIdClientText());
                orderDAO.insertOrder(orderView.getIdClientText(),orderView.getidOrderText());

            }else{
                if(understockFlag == 0){
                    orderView.showWarning("Not enough items in stock");
                }
            }
            orderView.refresh(orderDetailDAO.findAllByIdAndClient(orderView.getIdClientText(),orderView.getidOrderText()));
            productView.refresh(productDAO.findAll());

        }
    }

    private class ShowListener implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            System.out.println("Show Pressed");

            List<OrderDetail> orderDetailList = orderDetailDAO.findAllByIdAndClient(orderView.getIdClientText(),orderView.getidOrderText());
            if(!orderDetailList.isEmpty()) {
                orderView.refresh(orderDetailList);
            }else{
                orderView.showWarning("No available data!");
                orderView.refresh(nullList);
            }
        }
    }
    private class createBillListener implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            System.out.println("Create text bill pressed");


            try {
                businessLogic.createTextBill(orderView.getIdClientText(),orderView.getidOrderText());
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }
        }
    }



}
