package DAO;

import Controllers.ControllerOrder;
import Model.OrderDetail;
import connection.ConnectionFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class OrderDetailDAO extends AbstractDAO<OrderDetail> {


    public OrderDetailDAO() {

    }

    private String createInsertQuery(){
        StringBuilder sb = new StringBuilder();

        sb.append("INSERT INTO ");
        sb.append("OrderDetail ");
        sb.append("(idProduct,idOrder,orderQuantity,idClient)");
        sb.append(" VALUES (?,?,?,?)");

        return  sb.toString();
    }
    private String createSelectQuery(){
        StringBuilder sb = new StringBuilder();

        sb.append("SELECT * ");
        sb.append("FROM ");
        sb.append("OrderDetail ");
        sb.append("WHERE idClient = ? AND idOrder = ?");

        return  sb.toString();
    }

    public List<OrderDetail> selectOrderDetail(int idClient,int idOrder){


        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet  resultSet = null;
        String query = createSelectQuery();
        System.out.println(query);
        try {

            conn= ConnectionFactory.getConnection();
            statement = conn.prepareStatement(query);
            statement.setInt(1,idClient);
            statement.setInt(2,idOrder);
            resultSet = statement.executeQuery();

            return createObjects(resultSet);

        }catch ( Exception e){
            LOGGER.log(Level.WARNING,  "OrderDetailDAO:selectOrderDetail "+e.getMessage());
        }
        finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(conn);
        }
        return null;
    }
    public void inserOrderDetail(int idProduct,int idOrder,int orderQuantity,int idClient){

        System.out.println(idProduct+" "+idOrder+" "+orderQuantity+" "+idClient);

        Connection conn = null;
        PreparedStatement statement = null;
        String query = createInsertQuery();
        System.out.println(query);
        try {

            conn= ConnectionFactory.getConnection();
            statement = conn.prepareStatement(query);
            statement.setInt(1,idProduct);
            statement.setInt(2,idOrder);
            statement.setInt(3,orderQuantity);
            statement.setInt(4,idClient);
            statement.executeUpdate();

        }catch ( Exception e){
            LOGGER.log(Level.WARNING,  "OrderDetailDAO:insertOrderDetail "+e.getMessage());
        }
        finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(conn);
        }
    }

    private String queryOrderAndClient(){

        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM ");
        sb.append("OrderDetail");
        sb.append(" WHERE idClient = ? and idOrder = ?");

        return sb.toString();
    }

    public List<OrderDetail> findAllByIdAndClient(int idClient, int idOrder ) {

        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = queryOrderAndClient();
        System.out.println(query);
        try {
            System.out.println(query);
            conn = ConnectionFactory.getConnection();
            System.out.println(conn);
            statement = conn.prepareStatement(query);
            statement.setInt(1,idClient);
            statement.setInt(2,idOrder);
            resultSet = statement.executeQuery();

            return super.createObjects(resultSet);
        }catch (SQLException e){
            LOGGER.log(Level.WARNING, "OrderDetailDAO:findAllByIdAndClient "+e.getMessage());
        }finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(conn);
        }
        return  null;


    }


    public static void main(String[] args) {
        OrderDetailDAO ordDet= new OrderDetailDAO();

        ordDet.findAllByIdAndClient(1,1);
    }

}
