package DAO;

import Model.Order;
import connection.ConnectionFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.logging.Level;

public class OrderDAO extends AbstractDAO<Order> {


    public OrderDAO(){

    }

    private String createInsertQuery(){
        StringBuilder sb = new StringBuilder();

        sb.append("INSERT INTO ");
        sb.append("hw3.Order ");
        sb.append("(id,idClient)");
        sb.append(" VALUES (?,?)");

        return  sb.toString();
    }

    public void insertOrder(int clientId,int orderId){

        Connection conn = null;
        PreparedStatement statement = null;
        String query = createInsertQuery();
        System.out.println(query);
        try {

            conn= ConnectionFactory.getConnection();
            statement = conn.prepareStatement(query);
            statement.setInt(1,orderId);
            statement.setInt(2,clientId);
            statement.executeUpdate();

        }catch ( Exception e){
            LOGGER.log(Level.WARNING,  "OrderDAO:insertOrder "+e.getMessage());
        }
        finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(conn);
        }
    }


    public static void main(String[] args) {
        OrderDAO ord = new OrderDAO();

        ord.insertOrder(1,1);
    }
}
