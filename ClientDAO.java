package DAO;

import Model.Client;
import connection.ConnectionFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;

public class ClientDAO extends AbstractDAO<Client> {

    //uses crud from superclas IMPLEMET THEM

    private List<Integer> idList= new ArrayList<Integer>();

    public ClientDAO(){

        System.out.println("Gathering client idList ...");
        List<Client> clientsData= super.findAll();

        for (Client client: clientsData) {
            idList.add(client.getId());
        }
    }

    private String createInsertQuery(){
        StringBuilder sb = new StringBuilder();

        sb.append("INSERT INTO ");
        sb.append("Client ");
        sb.append("(id,firstName,lastName)");
        sb.append(" VALUES (?,?,?)");

        return  sb.toString();
    }

    private String createUpdateQuery(){
        StringBuilder sb = new StringBuilder();

        sb.append("UPDATE ");
        sb.append("Client ");
        sb.append("SET ");
        sb.append("firstName=?,lastName=?");
        sb.append("WHERE id=?");

        return  sb.toString();
    }

    public void updateClient(int id,String firstName,String lastName){

        Connection conn = null;
        PreparedStatement statement = null;
        String query = createUpdateQuery();
        System.out.println(query);
        System.out.println(firstName+ " "+lastName+" ");
        try {

            conn= ConnectionFactory.getConnection();
            statement = conn.prepareStatement(query);
            statement.setString(1,firstName);
            statement.setString(2,lastName);
            statement.setInt(3,id);

            statement.executeUpdate();

        }catch ( Exception e){
            LOGGER.log(Level.WARNING,  "ClientDAO:updateClient "+e.getMessage());
        }
        finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(conn);
        }
    }

    public void insertClient(String firstName,String lastName){

        Connection conn = null;
        PreparedStatement statement = null;
        int randomId = generateRandomId();
        String query = createInsertQuery();
        System.out.println(query);
        System.out.println(firstName+ " "+lastName+" "+randomId);
        try {

            conn= ConnectionFactory.getConnection();
            statement = conn.prepareStatement(query);
            statement.setInt(1,randomId);
            statement.setString(2,firstName);
            statement.setString(3,lastName);
            statement.executeUpdate();

        }catch ( Exception e){
            LOGGER.log(Level.WARNING,  "ClientDAO:insertClient "+e.getMessage());
        }
        finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(conn);
        }
    }

    private int generateRandomId(){

        Random randomId = new Random();
        int foundFlag = 0;
        int randomValue = 0;

        while(foundFlag != 1){
            randomValue = randomId.nextInt(100);
            if(!idList.contains(randomValue)){
                foundFlag = 1;
            }
        }

        return randomValue;

    }

    //TODO: create only student specific queries;
}
