package connection;

import javax.xml.transform.Result;
import java.net.ConnectException;
import java.sql.*;
import java.util.logging.Logger;

public class ConnectionFactory {

    private static  final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName());
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String DBURL = "jdbc:mysql://localhost:3306/hw3";
    private static final String USER = "root";
    private static final String PASS = "root";

    private static ConnectionFactory singleInstance = new ConnectionFactory();

    private ConnectionFactory(){
        try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static Connection createConnection(){

        Connection conn=null;

        try {
            conn = DriverManager.getConnection(DBURL,USER,PASS);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return  conn;

    }

    public static Connection getConnection(){

        Connection conn=null;

        if(conn == null){
            conn = createConnection();
        }

       return conn;
    }
    public  static void close(Connection connection){

        try {
            System.out.println("Closing connection ... ");
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static void close(Statement statement){

        try {
            System.out.println("Closing statement ...");
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static void close(ResultSet resultSet){
        try {
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        ConnectionFactory.createConnection();
        System.out.println(ConnectionFactory.getConnection());

        String sql = "SELECT * FROM client";
        PreparedStatement stmt= null;
        try {
            Connection conn = getConnection();
            stmt = conn.prepareStatement(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }


        try {
            ResultSet rs= stmt.executeQuery();

            while(rs.next()){

                int id=rs.getInt("id");
                String namE = rs.getString("firstName");

                System.out.println(id+"\n"+namE);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }



    }

}
