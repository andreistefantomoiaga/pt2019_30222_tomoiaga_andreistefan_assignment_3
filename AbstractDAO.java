package DAO;

import Model.Client;
import connection.ConnectionFactory;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

//TODO: create crud operations;
public class AbstractDAO<T> {

    protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

    private final Class<T> type;

    @SuppressWarnings("unchecked")
    public AbstractDAO() {
        this.type = (Class<T>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    private String createSelectQuery(String field){

        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE "+ field+ " = ?;");

        return sb.toString();
    }

    private String createSelectAllQuery(){

        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append("* ");
        sb.append("FROM ");
        sb.append(type.getSimpleName());

        return sb.toString();
    }

    private String createDeleteQuery(String field){
        StringBuilder sb = new StringBuilder();
        sb.append("DELETE ");
        sb.append("FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE "+ field +" = ?");

        return  sb.toString();
    }



    private String createUpdateQuery(String field){
        StringBuilder sb = new StringBuilder();

        sb.append("UPDATE ");
        sb.append(type.getSimpleName());
        sb.append(" SET ");
        sb.append( field + " = ?");
        sb.append(" WHERE id = ?");

        return  sb.toString();
    }


    public T findById(int id){
        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet  resultSet = null;
        String query = createSelectQuery("id");
        System.out.println(query);

        try {
            conn = ConnectionFactory.getConnection();
            statement = conn.prepareStatement(query);
            statement.setInt(1,id);
            resultSet = statement.executeQuery();

            return createObjects(resultSet).get(0);
        }catch (SQLException e){
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById "+e.getMessage());
        }finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(conn);
        }
        return  null;
    }

    public List<T> findAll(){
        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet  resultSet = null;
        String query = createSelectAllQuery();
        System.out.println(query);

        try {
            System.out.println(query);
            conn = ConnectionFactory.getConnection();
            System.out.println(conn);
            statement = conn.prepareStatement(query);
            resultSet = statement.executeQuery();

            return createObjects(resultSet);
        }catch (SQLException e){
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findAll "+e.getMessage());
        }finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(conn);
        }
        return  null;
    }


    public void deleteById(int id){

        Connection conn = null;
        PreparedStatement statement = null;
        String query = createDeleteQuery("id");

        try {
            conn = ConnectionFactory.getConnection();
            statement = conn.prepareStatement(query);
            statement.setInt(1,id);
            statement.executeUpdate();

        }catch ( SQLException e){
            LOGGER.log(Level.WARNING, type.getName() + "DAO:deleteById "+e.getMessage());
        }finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(conn);

        }
    }

    public List<T> createObjects(ResultSet resultSet){
        List<T> list = new ArrayList<T>();


        try {

            while (resultSet.next()) {
                T instance = type.newInstance();

                for (Field field : type.getDeclaredFields()) {
                    Object value = resultSet.getObject(field.getName());
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                    Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(instance, value);

                }
                list.add(instance);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }catch(InstantiationException e){
            e.printStackTrace();
        }catch (IllegalAccessException e){
            e.printStackTrace();
        } catch (IntrospectionException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        return list;

    }

//    public static void main(String[] args) {
//        AbstractDAO<Client> c= new ClientDAO();
//
//        List<Client> obj =c.findAll();
//
//        for (Client cf: obj
//             ) {
//
//            System.out.println(cf.getFirstName());
//        }
//
//    }
}
