package DAO;

import Model.Client;
import Model.Product;
import connection.ConnectionFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;

public class ProductDAO extends AbstractDAO<Product> {

    private List<Integer> idList= new ArrayList<Integer>();

    public ProductDAO(){

        System.out.println("Gathering product idList ... ");
        List<Product> productsData= super.findAll();

        for (Product product: productsData) {
            idList.add(product.getId());
        }
    }

    private String createInsertQuery(){
        StringBuilder sb = new StringBuilder();

        sb.append("INSERT INTO ");
        sb.append("Product ");
        sb.append("(id,productName,quantity)");
        sb.append(" VALUES (?,?,?)");

        return  sb.toString();
    }

    private String createUpdateQuery(){
        StringBuilder sb = new StringBuilder();

        sb.append("UPDATE ");
        sb.append("Product ");
        sb.append("SET ");
        sb.append("productName=?,quantity=? ");
        sb.append("WHERE id=?");

        return  sb.toString();
    }

    public void updateProduct(int id, String productName, int quantity){

        Connection conn = null;
        PreparedStatement statement = null;
        String query = createUpdateQuery();
        System.out.println(query);
        try {
            System.out.println(query);
            conn= ConnectionFactory.getConnection();
            statement = conn.prepareStatement(query);
            statement.setString(1,productName);
            statement.setInt(2,quantity);
            statement.setInt(3,id);

            statement.executeUpdate();

        }catch ( Exception e){
            LOGGER.log(Level.WARNING,  "ProductDAO:updateProduct "+e.getMessage());
        }
        finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(conn);
        }
    }

    public void insertProduct(String productName, int quantity){

        Connection conn = null;
        PreparedStatement statement = null;
        int randomId = generateRandomId();
        String query = createInsertQuery();
        System.out.println(query);
        try {

            conn= ConnectionFactory.getConnection();
            statement = conn.prepareStatement(query);
            statement.setInt(1,randomId);
            statement.setString(2,productName);
            statement.setInt(3,quantity);
            statement.executeUpdate();

        }catch ( Exception e){
            LOGGER.log(Level.WARNING,  "ProductDAO:insertProduct "+e.getMessage());
        }
        finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(conn);
        }

    }


    private int generateRandomId(){

        Random randomId = new Random();
        int foundFlag = 0;
        int randomValue = 0;

        while(foundFlag != 1){
            randomValue = randomId.nextInt(100);
            if(!idList.contains(randomValue)){
                foundFlag = 1;
            }
        }

        return randomValue;

    }
}
