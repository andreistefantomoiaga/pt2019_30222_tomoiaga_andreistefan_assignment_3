package Model;

import java.util.ArrayList;
import java.util.List;

public class OrderContainer {

    private List<Product> productList;

    public OrderContainer(){
        productList = new ArrayList<Product>();
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public void addProduct(Product product){

        productList.add(product);
    }
}
