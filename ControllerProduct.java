package Controllers;

import DAO.ProductDAO;
import PresentationLayer.ProductView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//TODO : not showing the jtable???


public class ControllerProduct {

    private ProductView productView;
    private ProductDAO productDAO;


    public ControllerProduct(ProductView productView, ProductDAO productDAO){

        this.productView = productView;
        this.productDAO = productDAO;
        productView.refresh(productDAO.findAll());


        //listeners methods:
        productView.viewProducts(new ViewAllListener());
        productView.addProduct( new addListener());
        productView.editProduct(new editListener());
        productView.deleteProduct(new deleteListener());

    }

    public static void main(String[] args) {

        ControllerProduct ctrl = new ControllerProduct(new ProductView(),new ProductDAO());
    }

    private class addListener implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            productDAO.insertProduct(productView.getProductNameFromTF(),productView.getQuantity());
            productView.refresh(productDAO.findAll());
        }
    }
    private class editListener implements ActionListener{

        public void actionPerformed(ActionEvent e) {

            productDAO.updateProduct(productView.getId(),productView.getProductNameFromTF(),productView.getQuantity());
            productView.refresh(productDAO.findAll());
        }
    }
    private class deleteListener implements ActionListener{

        public void actionPerformed(ActionEvent e) {

            productDAO.deleteById(productView.getId());
            productView.refresh(productDAO.findAll());
        }
    }
    private class ViewAllListener implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            productView.refresh(productDAO.findAll());
        }
    }

    public ProductView getProductView() {
        return productView;
    }
}
