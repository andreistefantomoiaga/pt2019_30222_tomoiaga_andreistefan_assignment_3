package PresentationLayer;

import Model.Client;
import Model.OrderDetail;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class OrderView extends JFrame {

    JPanel pane = new JPanel(new BorderLayout(5,5));

    JPanel top = new JPanel();
    JPanel center = new JPanel();

    JLabel idCLab = new JLabel("Client ID: ");
    JLabel idPLab = new JLabel("Product ID: ");
    JLabel quantLab = new JLabel("Quantity: ");
    JLabel idOrderLab = new JLabel("Order ID: ");

    JTextField idClient = new JTextField("2");
    JTextField idProduct = new JTextField("12");
    JTextField quantity = new JTextField("100");
    JTextField idOrder = new JTextField("3");
    JButton orderBtn = new JButton("Order");
    JButton showBtn = new JButton("Show Receipt Items");
    JButton createBill = new JButton("Create text bill");

    JTable orderTable;
    JScrollPane tablePane;


    public OrderView(){

        init();
        others();
        this.setSize(800,600);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        System.out.println(dim.width);
        this.setLocation(1070,dim.height/2 - this.getSize().height/2);
        this.setVisible(true);


    }

    private void init(){

        top.add(idCLab);
        top.add(idClient);
        top.add(idPLab);
        top.add(idProduct);
        top.add(quantLab);
        top.add(quantity);
        top.add(idOrderLab);
        top.add(idOrder);
        top.add(orderBtn);
        top.add(showBtn);
        top.add(createBill);
        pane.add(top,BorderLayout.PAGE_START);
        pane.add(center,BorderLayout.CENTER);
        add(pane);

    }
    private void others(){
        center.setBorder(BorderFactory.createLineBorder(Color.BLACK));
    }

    public void refresh(List<OrderDetail> all) throws NullPointerException{

        System.out.println("Clicked");
        pane.removeAll();
        center.removeAll();


        orderTable  = createTable(all);
        tablePane = new JScrollPane(orderTable);
        center.add(tablePane);
        init();

        pane.revalidate();
        pane.repaint();

    }

    private JTable createTable(List<OrderDetail> objects){

        //display Orders;

        List<String> columnNames = new ArrayList<String>();
        List<String[]> data = new ArrayList<String[]>();

        for (Field field: OrderDetail.class.getDeclaredFields()) {
            columnNames.add(field.getName());
            System.out.println(field.getName());
        }

        if(!objects.isEmpty()) {
            for (OrderDetail ordDet : objects) {
                data.add(new String[]{String.valueOf(ordDet.getIdOrder()), String.valueOf(ordDet.getIdProduct()), String.valueOf(ordDet.getOrderQuantity())});
            }
        }

        TableModel tableModel= new DefaultTableModel(data.toArray(new Object[][]{}),columnNames.toArray());
        JTable retunedJtable =new JTable(tableModel);

        return retunedJtable;

    }
     public void showWarning(String message){

         JOptionPane.showMessageDialog(this, message);
     }



    public void orderEvent(ActionListener e){
        orderBtn.addActionListener(e);
    }
    public void showEvent (ActionListener e) { showBtn.addActionListener(e);}
    public void createBillEvent (ActionListener e) { createBill.addActionListener(e);}


    public static void main(String[] args) {
        OrderView ov = new OrderView();
    }



    public int getIdClientText(){
        return Integer.parseInt(idClient.getText());
    }

    public int getIdProductText(){
        return Integer.parseInt(idProduct.getText());
    }
    public int quantityText(){
        return Integer.parseInt(quantity.getText());
    }
    public int getidOrderText(){
        return Integer.parseInt(idOrder.getText());
    }

}
