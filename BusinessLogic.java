package BusinessLogic;

import DAO.OrderDetailDAO;
import DAO.ProductDAO;
import Model.OrderDetail;
import Model.Product;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

public class BusinessLogic {

    private ProductDAO productDAO;
    private OrderDetailDAO orderDetailDAO;
    private Product product;

    public BusinessLogic(){

        productDAO = new ProductDAO();
        orderDetailDAO = new OrderDetailDAO();
    }

    public void createTextBill(int idClient,int idOrder) throws FileNotFoundException, UnsupportedEncodingException {

        PrintWriter writer = new PrintWriter("bill.txt", "UTF-8");

        List<OrderDetail> orderDetailItems = orderDetailDAO.selectOrderDetail(idClient,idOrder);
        writer.println("Order: "+idOrder);
        writer.println("Client: "+idClient);

        Product product;

        for (OrderDetail ordDet: orderDetailItems) {

            product = productDAO.findById(ordDet.getIdProduct());

            writer.println("Item name: "+product.getProductName()+" Quantity:"+ordDet.getOrderQuantity());
        }

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();


        writer.println("Print date:"+dtf.format(now));
        //        writer.println("The first line");
//        writer.println("The second line");
        writer.close();

    }

    public int underStock(int quantity,int productId){

        try {
            product = productDAO.findById(productId);
            int actualQuantity = product.getQuantity();

            if(actualQuantity < quantity){
                System.out.println("BusinessLogic:underStock Not enough items in stock");
                return 0;
            }else{
                System.out.println("BusinessLogic:underStock Enough items in stock");
                decrementStock(quantity,productId);
                return 1;
            }
        }catch (IndexOutOfBoundsException e){
            System.out.println("BusinessLogic:underStock IndexOutOfBoundsException");
            return 2;
        }


    }
    public void decrementStock(int orderedQuantity,int productId){

        try {
            product = productDAO.findById(productId);
            int actualQuantity = product.getQuantity();
            System.out.println("BusinessLogic:decrementStock");
            productDAO.updateProduct(productId,product.getProductName(),actualQuantity-orderedQuantity);
        }catch (NullPointerException e){
            System.out.println("BusinessLogic:incrementStock item not found");
        }

    }
    public void incrementStock(int addedQuantity,int productId){

        try {
             product = productDAO.findById(productId);
            int actualQuantity = product.getQuantity();
            System.out.println("BusinessLogic:incrementStock");
            productDAO.updateProduct(productId, product.getProductName(), actualQuantity + addedQuantity);
        }catch (NullPointerException e){
            System.out.println("BusinessLogic:incrementStock item not found");
        }
    }

    public void setProductDAO(ProductDAO productDAO) {
        this.productDAO = productDAO;
    }

    public ProductDAO getProductDAO() {
        return productDAO;
    }

    public static void main(String[] args) {
         BusinessLogic bl = new BusinessLogic();
        try {
            bl.createTextBill(1,3);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
